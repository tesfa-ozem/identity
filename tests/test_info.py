import httpx
import pytest
from fastapi import FastAPI, status

from settings import AuthSettings


@pytest.fixture
def endpoint_url(fastapi_app: FastAPI) -> str:
    return fastapi_app.url_path_for(name="info")


async def test_info(
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    auth_settings: AuthSettings,
) -> None:
    response = await http_client.get(endpoint_url)
    assert response.status_code == status.HTTP_200_OK
    assert response.json() == {
        "publicKey": auth_settings.jwt_public_key,
    }
