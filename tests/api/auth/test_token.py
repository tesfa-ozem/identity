import datetime
import time
from unittest.mock import patch

import httpx
import jwt
import pytest
from common.auth.models import AuthTokenPayload
from fastapi import status
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from apps.auth._commands import REFRESH_TOKEN_COOKIE_NAME
from db.models import RefreshToken, User
from settings import AppSettings, AuthSettings


@pytest.fixture
def operation_id() -> str:
    return "auth_token"


@pytest.fixture
def grant_type() -> str:
    return "password"


@pytest.fixture
def requested_scopes() -> list[str]:
    return ["basic"]


async def test_no_user(
    grant_type: str,
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    requested_scopes: list[str],
) -> None:
    response = await http_client.post(
        endpoint_url,
        data={
            "grant_type": grant_type,
            "username": "Username",
            "password": " ",
            "scope": requested_scopes,
        },
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"detail": "Bad request", "code": "BAD_REQUEST"}


async def test_wrong_password(
    grant_type: str,
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    requested_scopes: list[str],
    user: User,
    user_password: str,
) -> None:
    response = await http_client.post(
        endpoint_url,
        data={
            "grant_type": grant_type,
            "username": user.username,
            "password": user_password + " ",
            "scope": ["basic"],
        },
    )
    assert response.status_code == status.HTTP_400_BAD_REQUEST
    assert response.json() == {"detail": "Bad request", "code": "BAD_REQUEST"}


async def test_base_case(
    grant_type: str,
    endpoint_url: str,
    http_client: httpx.AsyncClient,
    user: User,
    user_password: str,
    requested_scopes: list[str],
    http_domain: str,
    session: AsyncSession,
    app_settings: AppSettings,
    auth_settings: AuthSettings,
) -> None:
    with patch.object(app_settings, "domain", http_domain):
        response = await http_client.post(
            endpoint_url,
            data={
                "grant_type": grant_type,
                "username": user.username,
                "password": user_password,
                "scope": requested_scopes,
            },
        )

    assert response.status_code == status.HTTP_200_OK
    data = response.json()
    assert data["token_type"] == "Bearer"
    assert data["expires_in"] == int(
        auth_settings.access_token_lifetime.total_seconds()
    )

    access_token = data["access_token"]
    claims_raw = jwt.decode(
        access_token,
        key=auth_settings.jwt_public_key,
        algorithms=[auth_settings.jwt_algorithm],
    )
    claims = AuthTokenPayload.parse_obj(claims_raw)

    assert claims.sub == str(user.id)
    assert claims.iat == pytest.approx(int(time.time()), rel=1)
    assert (
        claims.exp == claims.iat + auth_settings.access_token_lifetime.total_seconds()
    )
    assert claims.username == user.username
    assert claims.scopes == requested_scopes
    assert claims.permissions == []

    refresh_token = response.cookies.get(
        REFRESH_TOKEN_COOKIE_NAME,
        domain=http_domain,
    )
    assert refresh_token

    refresh_token_model: RefreshToken = await session.scalar(
        select(RefreshToken).where(RefreshToken.id == refresh_token)
    )
    assert refresh_token_model

    assert datetime.datetime.now(
        tz=datetime.timezone.utc
    ) - refresh_token_model.created_at < datetime.timedelta(seconds=1)
