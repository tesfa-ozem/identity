from datetime import datetime, timedelta, timezone

import pytest
from common.tests import ApproxDateTime

from apps.auth.services import TokenService
from db.models import RefreshToken, User
from settings import AuthSettings


@pytest.fixture
async def refresh_token(
    user: User,
    token_service: TokenService,
) -> RefreshToken:
    return await token_service.generate_refresh_token(user=user, scopes=[])


async def test_refresh_token_renewal(
    auth_settings: AuthSettings,
    refresh_token: RefreshToken,
    token_service: TokenService,
) -> None:
    refresh_token.valid_until = datetime.now(tz=timezone.utc) + timedelta(hours=1)
    new_token = await token_service.renew_refresh_token(refresh_token)

    assert new_token.valid_until == ApproxDateTime(
        datetime.now(tz=timezone.utc) + auth_settings.refresh_token_lifetime,
        abs=timedelta(seconds=1),
    )
