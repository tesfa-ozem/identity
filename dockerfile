FROM python:3.10-slim as build

RUN pip install pdm
COPY ./pyproject.toml ./pdm.lock ./
RUN pdm export --prod -f requirements -o requirements.txt

FROM python:3.10-slim as final

WORKDIR /app
ENV PYTHONPATH=$PYTHONPATH:src
COPY --from=build requirements.txt .
RUN pip install --upgrade pip && pip install -r requirements.txt --no-cache-dir

COPY ./src ./src
COPY alembic.ini ./

ENTRYPOINT ["uvicorn", "app:create_app", "--factory", "--loop", "uvloop", "--host", "0.0.0.0", "--port", "8000"]
