import contextlib
from typing import AsyncIterator

from sqlalchemy.ext.asyncio import AsyncSession

from .base import async_sessionmaker


@contextlib.asynccontextmanager
async def get_session() -> AsyncIterator[AsyncSession]:
    async with async_sessionmaker.begin() as session:
        yield session
