import datetime
import uuid

from sqlalchemy import Column, DateTime, String, func
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import Mapped

from db import Base


class User(Base):
    __tablename__ = "user"

    id: Mapped[uuid.UUID] = Column(
        postgresql.UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )
    username: str = Column(
        String(100),
        nullable=False,
        unique=True,
    )
    password_hash: str = Column(String(250), nullable=False)
    created_at: datetime.datetime = Column(
        DateTime(timezone=True),
        nullable=False,
        server_default=func.now(),
    )
