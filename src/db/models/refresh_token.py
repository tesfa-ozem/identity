from __future__ import annotations

import datetime
import uuid
from typing import TYPE_CHECKING

from sqlalchemy import ARRAY, Column, DateTime, ForeignKey, String, func
from sqlalchemy.dialects import postgresql
from sqlalchemy.orm import relationship

from db import Base

if TYPE_CHECKING:
    from .users import User


class RefreshToken(Base):
    __tablename__ = "refresh_token"

    id: uuid.UUID = Column(
        postgresql.UUID(as_uuid=True),
        primary_key=True,
        default=uuid.uuid4,
    )
    user_id: uuid.UUID = Column(ForeignKey("user.id"), nullable=False)
    scopes: tuple[str] = Column(ARRAY(String, as_tuple=True), nullable=False)
    user: User = relationship("User")
    created_at: datetime.datetime = Column(
        DateTime(timezone=True),
        nullable=False,
        server_default=func.now(),
    )
    valid_until: datetime.datetime = Column(
        DateTime(timezone=True),
        nullable=False,
    )
