from typing import Annotated

from aioinject import Inject
from aioinject.ext.fastapi import InjectMiddleware, inject
from common.web.exceptions import APIException, api_exception_handler
from fastapi import FastAPI
from sqlalchemy.ext.asyncio import AsyncSession
from starlette.middleware.cors import CORSMiddleware

from apps import auth, info, users
from di.container import create_container
from gql.app import create_gql_app
from settings import AppSettings


def create_app() -> FastAPI:
    settings = AppSettings()
    app = FastAPI()

    app.include_router(auth.router, prefix="/auth", tags=["auth"])
    app.include_router(users.router, prefix="/users", tags=["users"])
    app.include_router(info.router, prefix="/info", tags=["info"])

    app.add_middleware(
        CORSMiddleware,
        allow_origins=[settings.domain],
        allow_methods="*",
    )
    app.add_middleware(InjectMiddleware, container=create_container())
    app.mount("/graphql/", app=create_gql_app())

    @app.get("/healthcheck")
    @inject
    async def healthcheck(session: Annotated[AsyncSession, Inject]) -> None:
        await session.execute("select 1")

    app.add_exception_handler(APIException, api_exception_handler)

    return app
