from common.web.exceptions import BadRequest


class EntityAlreadyExists(BadRequest):
    code = "ENTITY_ALREADY_EXISTS"
    detail = "Entity already exists"
