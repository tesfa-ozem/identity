import strawberry


@strawberry.input
class UserCreateInput:
    username: str
    password: str
