from typing import Annotated

import strawberry
from aioinject import Inject
from aioinject.ext.strawberry import inject

from apps.users.dto import UserCreateDto
from apps.users.services import UserService

from .inputs import UserCreateInput
from .results import UserCreateResult


@strawberry.type
class UsersMutation:
    @strawberry.mutation
    @inject
    async def user_create(
        self, input: UserCreateInput, user_service: Annotated[UserService, Inject]
    ) -> UserCreateResult:
        user = await user_service.create_user(dto=UserCreateDto.from_orm(input))
        return UserCreateResult(
            user=user,
        )
