import strawberry.federation


@strawberry.federation.interface(
    directives=["shareable"],
)
class Error:
    message: str
