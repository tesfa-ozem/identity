from __future__ import annotations

import dataclasses
import uuid
from functools import cached_property

from common.auth.models import AuthTokenPayload
from common.settings import get_settings
from common.web.auth import AuthenticationError, authenticate_user
from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.dataloader import DataLoader
from strawberry.types import Info as StrawberryInfo

from db.models import User
from settings import AuthSettings


@dataclasses.dataclass
class Loaders:
    user_by_id: DataLoader[uuid.UUID | str, User | None]


@dataclasses.dataclass
class Context:
    request: Request | WebSocket
    response: Response | None
    loaders: Loaders

    @cached_property
    def user(self) -> AuthTokenPayload:
        if not isinstance(self.request, Request):
            raise AuthenticationError

        settings = get_settings(AuthSettings)
        return authenticate_user(
            token=self.request.headers.get("Authorization", ""),
            public_key=settings.jwt_public_key,
            algorithm=settings.jwt_algorithm,
        )

    @cached_property
    def maybe_user(self) -> AuthTokenPayload | None:
        try:
            return self.user
        except AuthenticationError:
            return None


class Info(StrawberryInfo[Context, None]):
    pass
