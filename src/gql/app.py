from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.asgi import GraphQL
from strawberry.dataloader import DataLoader

from .context import Context, Loaders
from .schema import schema
from .users.dataloaders import load_user_by_id


class GraphQLApp(GraphQL):
    async def get_context(
        self,
        request: Request | WebSocket,
        response: Response | None = None,
    ) -> Context:
        return Context(
            request=request,
            response=response,
            loaders=Loaders(
                user_by_id=DataLoader(load_user_by_id),
            ),
        )


def create_gql_app() -> GraphQL:
    app = GraphQLApp(
        schema=schema,
    )

    return app
