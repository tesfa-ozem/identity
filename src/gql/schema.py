import strawberry.federation
from aioinject.ext.strawberry import make_container_ext
from strawberry.extensions import ParserCache, ValidationCache
from strawberry.tools import merge_types

from di.container import create_container
from gql.users.mutation import UsersMutation
from gql.users.query import UsersQuery

Query = merge_types(name="Query", types=(UsersQuery,))

Mutation = merge_types(name="Mutation", types=(UsersMutation,))

schema = strawberry.federation.Schema(  # type: ignore
    query=Query,
    mutation=Mutation,
    extensions=[
        make_container_ext(create_container()),
        ParserCache(),
        ValidationCache(),
    ],
    enable_federation_2=True,
)
