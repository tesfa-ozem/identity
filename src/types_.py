from typing import MutableSequence, TypeVar

T = TypeVar("T")
SequenceOrTuple = tuple[T, ...] | MutableSequence[T]
