import uuid

from common.schema.users import UserSchema
from common.web.exceptions import APIException, NotFound
from fastapi import Depends
from starlette import status

from apps.users.dto import UserCreateDto
from apps.users.exceptions import UserAlreadyExists
from apps.users.schema import UserCreateSchema
from apps.users.services import UserService


class UserCreateCommand:
    def __init__(
        self,
        user_service: UserService = Depends(),
    ):
        self.user_service = user_service

    async def execute(self, user_schema: UserCreateSchema) -> UserSchema:
        try:
            user = await self.user_service.create_user(
                UserCreateDto.from_orm(user_schema)
            )
        except UserAlreadyExists as e:
            raise APIException(
                status_code=status.HTTP_400_BAD_REQUEST,
                code="ENTITY_ALREADY_EXISTS",
                detail="User with such credentials already exists",
            ) from e
        return UserSchema.from_orm(user)


class UserRetrieveCommand:
    def __init__(
        self,
        user_service: UserService = Depends(),
    ):
        self.user_service = user_service

    async def execute(self, user_id: uuid.UUID) -> UserSchema:
        user = await self.user_service.get_user(entity_id=user_id)
        if user is None:
            raise NotFound
        return UserSchema.from_orm(user)
