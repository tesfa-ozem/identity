import datetime
import uuid

from pydantic import SecretStr

from schema import BaseSchema


class UserCreateSchema(BaseSchema):
    username: str
    password: SecretStr


class UserSchema(BaseSchema):
    id: uuid.UUID
    username: str
    created_at: datetime.datetime
