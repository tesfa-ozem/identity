from pydantic import SecretStr

from schema import BaseDto


class UserCreateDto(BaseDto):
    username: str
    password: SecretStr
